# Multimedia-Repository-Web #
* Version: **v.0.0.2**
-----------------------
### 1. Application Info ###

REST Web services application based on MEAN stack: **M**ongoDB, **E**xpress, **A**ngularjs, **N**ode.js

Using a NGinx web/proxy server

| Component       | Ubuntu 14.04 installation
| ------------- | :-----
| 	`MongoDB`		|       http://docs.mongodb.org/master/tutorial/install-mongodb-on-ubuntu/
| 	`Express`		|       http://expressjs.com/es/
| 	`Angularjs`		|       https://angularjs.org/
| 	`Node.js`		|       https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server
| 	`Nginx`		|       https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-14-04-lts

##### IDE:
* http://sourceforge.net/projects/nodeclipse/?source=typ_redirect

>    	Instructions
>    	    	0. If you don't have, get latest Node.js http://www.nodejs.org/download/
>    	    	1. If you don't have, download & install latest JDK
>    	    	    	http://www.oracle.com/technetwork/java/javase/downloads/index.html
>    	    	    	For example "Java Platform (JDK) 7u21"
>    	    	2. Download Node Tool Suite (NTS) for your operating system (currently only Windows x64 & MacOS x64)
>    	    	    	http://www.nodeclipse.org/nts/
>    	    	3. Extract NTS.zip into folder where you keep our tools, e.g. D:\Progs\
>    	    	4. Open eclipse.exe from Eclipse folder, e.g. D:\Progs\Node-Tool-Suite-03-win64\eclipse\eclipse.exe
>    	    	5. If you have error messages like
>    	    	    	....\jre\....
>    	    	    	That means you don't have JDK installed (JRE is not enough).
>    	    	    	Reinstall JDK (see 1.) or use hint "how to configure Eclipse" https://github.com/Nodeclipse/eclipse-node-ide/blob/master/Hints.md#select-jvm-for-eclipse-instance
>    	    	6. You default Workspace (folder where you keep you projects) is ..\workspace,
>    	    	    	e.g. D:\Progs\Node-Tool-Suite-03-win64\workspace
>    	    	    	Change it to more suitable place
