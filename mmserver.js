var app_name_version = "multimedia-repository-app [v.0.0.5]";

var express = require('express');
var app = express();
var router = express.Router();
var bodyParser = require('body-parser');
var logger = require('morgan');
var jwt = require("jsonwebtoken");
var mongoose = require('mongoose'); // https://scotch.io/tutorials/using-mongoosejs-in-node-js-and-mongodb-applications
// logs
var Logs = require('./db/Logs');
var logs = new Logs();

// config file
var config = require('./config');

// server port we want to listen to
const PORT = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || config.port;

// database queries
mongoose.connect(config.database);

var DocumentsQueries = require('./db/DocumentsQueries');
var docs = new DocumentsQueries();

var UsersQueries = require('./db/UsersQueries');
var users = new UsersQueries(config.token_var);

// cloudinary connection
var Cloudinary = require('./db/Cloudinary');
var cvar = new Cloudinary(config.cloudname, config.api_key, config.api_secret);


///////////////////////////////////////////////////////////////////////////////
// MONITOR ////////////////////////////////////////////////////////////////////
if (config.monitoring_active) {
    var Monitor = require('./monitor');
    var vMonitoring = new Monitor();

    var freeMem = 0;
    var freeMemArr = [];
    var totalMem = 0;
    var totalMemArr = [];
    var loadavg;
    var loadavgArr = [];
    var appMem = 0;
    var appMemArr = [];
    var appProcessMem = 0;
    var appProcessMemArr = [];

    // initialize arrays
    for (var j=0; j<10; j++)
    {
        freeMemArr.push(0);
        totalMemArr.push(0);
        appMemArr.push(0);
        appProcessMemArr.push(0);
        loadavgArr.push([0, 0, 0]);
    }

    // PERIODIC TASKS - MONITOR
    var AsyncPolling = require('async-polling');

    var i = 0;

    var polling = AsyncPolling(function (end) {
            end(null, '#');
    }, 2500);

    polling.on('result', function (result) {
            freeMem = vMonitoring.getFreeMem();
            totalMem = vMonitoring.getTotalMem();
            loadavg = vMonitoring.getLoadavg();
            appMem = vMonitoring.getAppMemUsage();
            appProcessMem = vMonitoring.getAppProcessMemUsage("java");

            if (freeMemArr.length == 10)
            {
                freeMemArr.shift();
                totalMemArr.shift();
                loadavgArr.shift();
                appMemArr.shift();
                appProcessMemArr.shift();
            }

            freeMemArr.push(freeMem);
            totalMemArr.push(totalMem);
            loadavgArr.push(loadavg);
            appMemArr.push(appMem);
            appProcessMemArr.push(appProcessMem);
    });

    polling.on('error', function (error) {
            console.error('error:', error);
    });

    polling.run();
}


///////////////////////////////////////////////////////////////////////////////
// CONFIGURATION //////////////////////////////////////////////////////////////
app.use(bodyParser.json());                         // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(logger('dev'));                             // enables loggin
app.use(express.static(__dirname + '/public'));
app.set('secret_token_var', config.token_var);      // token_var

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});


// verify the request token
function verifyReqToken(req, res, next) {
    if (config.token_active) {
        logs.debug('Verifying request token for ... ' + req.method + ' >> ' + req.originalUrl);
        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['authorization'];

        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, app.get('secret_token_var'), function(err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                }
                else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });
        }
        else {
            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided / session expired'
            });
        }
    }
    else {
        next();
    }
}


///////////////////////////////////////////////////////////////////////////////
// REST API routes ////////////////////////////////////////////////////////////

// USERS:

// >>>> api/users
router.route('/api/users')
    // GET: get all users
    .get(function(req, res) {
        users.getAllUsers(req, res);
    })
    // POST: create new user
    .post(function(req, res) {
        users.createUser(req, res);
    });

app.use('/api/users', function(req, res, next) {
    verifyReqToken(req, res, next);
});


// >>>> api/users/:id
router.route('/api/users/:id')
    // GET: get user by id
    .get(function(req, res) {
        users.getUserById(req, res);
    })
    // PUT: update user by id
    .put(function(req, res) {
        users.updateUser(req, res);
    })
    // DELETE: delete user by ID
    .delete(function(req, res) {
        users.deleteUser(req, res);
    });

app.use('/api/users/:id', function(req, res, next) {
    verifyReqToken(req, res, next);
});


// >>>> api/users/auth
router.route('/api/auth')
    // POST: authentication
    .post(function(req, res) {
        users.doAuth(req, res);
    });


// DOCUMENTS (VIDEOS, IMGs, BOOKS ...):

// >>>> api/documents
router.route('/api/documents')
    // GET: get all
    .get(function(req, res) {
        docs.getAllDocuments(req, res);
    })
    // POST: create new document
    .post(function(req, res) {
        docs.createDocument(req, res);
    });

app.use('/api/documents', function(req, res, next) {
    verifyReqToken(req, res, next);
});


// >>>> api/documents/:id
router.route('/api/documents/:id')
    // GET: get usdocument by id
    .get(function(req, res) {
        docs.getDocumentById(req, res);
    })
    // PUT: update document by id
    .put(function(req, res) {
        res.send('Method not implemented');
    })
    // DELETE: delete document by ID
    .delete(function(req, res) {
        docs.deleteDocument(req, res);
    });

app.use('/api/documents/:id', function(req, res, next) {
    verifyReqToken(req, res, next);
});


// >>>> api/documents/images
router.route('/api/images')
    // GET: get all images
    .get(function(req, res) {
        //cvar.getAllImages(req, res);
        docs.getDocumentsByType('image', res);
    });

app.use('/api/images', function(req, res, next) {
    verifyReqToken(req, res, next);
});


// >>>> api/videos
router.route('/api/videos')
    // GET: get all videos
    .get(function(req, res) {
        docs.getDocumentsByType('video', res);
    });

app.use('/api/videos', function(req, res, next) {
    verifyReqToken(req, res, next);
});


// >>>> api/music
router.route('/api/music')
    // GET: get all music
    .get(function(req, res) {
        docs.getDocumentsByType('music', res);
    });

app.use('/api/music', function(req, res, next) {
    verifyReqToken(req, res, next);
});


// >>>> api/books
router.route('/api/books')
    // GET: get all books
    .get(function(req, res) {
        docs.getDocumentsByType('book', res);
    });

app.use('/api/books', function(req, res, next) {
    verifyReqToken(req, res, next);
});


// >>>> api/others
router.route('/api/others')
    // GET: get all others
    .get(function(req, res) {
        docs.getDocumentsByType('other', res);
    });

app.use('/api/others', function(req, res, next) {
    verifyReqToken(req, res, next);
});


// >>>> api/cloud/documents
router.route('/api/cloud/documents')
    // POST: create new document in Cloudinary
    .post(function(req, res) {
        cvar.createDocument(req, res);
    });

app.use('/api/cloud/documents', function(req, res, next) {
    verifyReqToken(req, res, next);
});


// >>>> api/cloud/document/:id
router.route('/api/cloud/document/:id')
    // DELETE: delete from cloudinary
    .delete(function(req, res) {
        cvar.deleteDocument(req, res);
    });

app.use('/api/cloud/document/:id', function(req, res, next) {
    verifyReqToken(req, res, next);
});


// MONITORING:
if (config.monitoring_active) {
    // >>>> /monitor/stop
    router.route('/monitor/stop')
        // GET: stop
        .get(function(req, res) {
            vMonitoring.stop();
            res.json({ "result" : "ok" });
        });

    app.use('/monitor/stop', function(req, res, next) {
        verifyReqToken(req, res, next);
    });


    // >>>> /monitor/data
    router.route('/monitor/data')
        // GET: get data
        .get(function(req, res) {
            var result = [];

            result.push({freeMemArr: freeMemArr});
            result.push({totalMemArr: totalMemArr});
            result.push({appMemArr: appMemArr});
            result.push({appProcessMemArr: appProcessMemArr});
            result.push({loadavg: loadavgArr});

            res.json({ "result": result});
        });

    app.use('/monitor/data', function(req, res, next) {
        verifyReqToken(req, res, next);
    });
}


// apply the routes to our application
app.use('/', router);
///////////////////////////////////////////////////////////////////////////////


// Angular manages the Frontend
app.use('/*', function(req, res){
    res.sendFile(__dirname + '/public/index.html');
});


// Run server and listen to port ...
app.listen(PORT, function() {
    // server port info
    logs.info('[' + app_name_version + '] listening on port ' + PORT + '...');
    logs.info('.............................');

    // check logs
    logs.error('Checking error logs ...');
    logs.warn('Checking warn logs ...');
    logs.info('Checking info logs ...');
    logs.debug('Checking debug logs ...');
    logs.log('verbose', 'Checking verbose logs ...');
    logs.log('silly', 'Checking silly logs ...');
    logs.info('.............................');

    // check database
    users.initialize(false);
    docs.initialize(false);
    cvar.initialize();
});
