/*
 * Monitoring functions
 */
// app version
var monitor_name_version = "multimedia-repository-monitor [v.0.0.2]";

// require's
var monitor = require("os-monitor");				// https://github.com/lfortin/node-os-monitor
var usage_app = require('usage');					// https://github.com/arunoda/node-usage
var sys = require('sys')										// used to execute commands on OS
var exec = require('child_process').exec;

// global vars
var freeMem = 0;
var totalMem = 0;
var loadavg;
var pid = process.pid;


// MONITOR
function Monitor(dbstr) {
    /*
    // start: advanced usage with configs.
    monitor.start({ 	delay: 2500, // interval in ms between monitor cycles
                   //freemem: 1000000000, // freemem under which event 'freemem' is triggered
                   //uptime: 1000000, // number of secs over which event 'uptime' is triggered
                   //critical1: 0.7, // loadavg1 over which event 'loadavg1' is triggered
                   //critical5: 0.7, // loadavg5 over which event 'loadavg5' is triggered
                   //critical15: 0.7, // loadavg15 over which event 'loadavg15' is triggered
                   //silent: false, // set true to mute event 'monitor'
                   stream: true, // set true to enable the monitor as a Readable Stream
                   //immediate: false // set true to execute a monitor cycle at start()
                  }); //.pipe(process.stdout);
    console.log(monitor_name_version + ' - started [1] ...');*/
}

// round value
function round(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

// define handler that will always fire every cycle
monitor.on('monitor', function(event) {
    //console.log(event.type, ' This event always happens on each monitor cycle!');
    /*
	  {
		  type: 'monitor', // event type
		  loadavg: [ 0.4599609375, 0.53076171875, 0.4990234375 ], // load average values for 1, 5, 15 minutes
		  uptime: 1614056, // os uptime in seconds
		  freemem: 241262592, // free memory available in bytes
		  totalmem: 2147483648, // total memory available in bytes
		  timestamp: 1394766898 // UNIX Timestamp
		}
	  */
    freeMem = round(((event.freemem / 1024) / 1024), 2);
    totalMem = round(((event.totalmem / 1024) / 1024), 2);
    loadavg = event.loadavg;
    //console.log(event);
});

// define handler for a too high 1-minute load average
monitor.on('loadavg1', function(event) {
    console.log(event.type, monitor_name_version + ' - Load average is exceptionally high!');
});

// define handler for a too low free memory
monitor.on('freemem', function(event) {
    console.log(event.type, monitor_name_version + ' - Free memory is very low!');
});

// define a throttled handler, using Underscore.js's throttle function (http://underscorejs.org/#throttle)
monitor.throttle('loadavg5', function(event) {
    // whatever is done here will not happen
    // more than once every 5 minutes(300000 ms)
}, monitor.minutes(5));

// change config while monitor is running
monitor.config({
    freemem: 0.3 // alarm when 30% or less free memory available
});


// PUBLIC METHODS
// stop monitor
Monitor.prototype.stop = function()
{
    if (monitor.isRunning())
    {
        monitor.stop();
        console.log(monitor_name_version + ' - Monitor stopped');
    }
    else
    {
        console.log(monitor_name_version + ' - Monitor already stopped');
    }
};

// restart
Monitor.prototype.start = function()
{
    monitor.start();
    console.log(monitor_name_version + ' - Monitor started [2] ...');
};

// check whether monitor is running or not
Monitor.prototype.isRunning = function()
{
    monitor.isRunning(); // -> true / false
};

// check whether monitor is running or not
Monitor.prototype.getFreeMem = function()
{
    return freeMem;
};

// check whether monitor is running or not
Monitor.prototype.getTotalMem = function()
{
    return totalMem;
};

// check whether monitor is running or not
Monitor.prototype.getLoadavg = function()
{
    return loadavg;
};

// process / app memory usage
var appMem = 0;
Monitor.prototype.getAppMemUsage = function()
{
    usage_app.lookup(pid, function(err, result) {
        /*
					RESPONSE FORMAT:

					{
						memory: 24256512,
						memoryInfo: { rss: 24256512, vsize: 4031799885824 },
						cpu: 100.00000000722578,
						cpuInfo:
						{
							pcpu: 100.00000000722578,
							pcpuUser: 10.34482758695439,
							pcpuSystem: 89.65517242027138,
							cpuTime: undefined
						}
					}
					*/
        appMem = round(((result.memory / 1024) / 1024), 2);
    });

    return appMem;
};

// process / app memory usage
var appProcessMem = 0;

function puts(error, stdout, stderr) {
    try {
        var res = stdout.replaceAll("\n", " ").split(" ");
        appProcessMem = round(( (parseInt(res[0]) + parseInt(res[13])) / 1024), 2);
    }
    catch(err) {
        console.log('ERROR: ' + err.message);
        appProcessMem = -1;
    }
}

String.prototype.replaceAll = function(target, replacement) {
    return this.split(target).join(replacement);
};

Monitor.prototype.getAppProcessMemUsage = function(pid_process)
{
    /*usage_app.lookup(pid_process, function(err, result) {
				appProcessMem = round(((result.memory / 1024) / 1024), 2);
		});*/
    exec("ps xao rss,%cpu,%mem,pid,ppid,pgid,user,comm | egrep '" + pid_process + "'", puts);
    console.log('return ==> ' + appProcessMem);
    return appProcessMem;
};


// exports module
module.exports = Monitor;
