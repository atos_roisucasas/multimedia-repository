var winston = require('winston');     // https://github.com/winstonjs/winston
// config file
var config = require('../config');

// CONFIGURATION //////////////////////////////////////////////////////////////
var twoDigit = '2-digit';
var options = {
  day: twoDigit,
  month: twoDigit,
  year: twoDigit,
  hour: twoDigit,
  minute: twoDigit,
  second: twoDigit
};

// log levels: // { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            level: 'silly',
            formatter: function(options) {
                var dateTimeComponents = new Date().toLocaleTimeString('en-us', options);
                var logMessage = dateTimeComponents + ' - ' + options.level.toUpperCase() + ': ' + options.message;
                return logMessage;
            }
        }) //,
        //new (winston.transports.File)({
        //    filename: config.logs_filename,
        //    level: 'silly'
        //})
    ]
});

// DocumentsQueries constructor
function Logs() {}


// PUBLIC FUNCTIONS ///////////////////////////////////////////////////////////

Logs.prototype.error = function(txt)
{
    logger.error('>> ' + txt);
};

Logs.prototype.warn = function(txt)
{
    logger.warn('>> ' + txt);
};

Logs.prototype.info = function(txt)
{
    logger.info('>> ' + txt);
};

Logs.prototype.debug = function(txt)
{
    logger.log('debug', '>> ' + txt);
};

Logs.prototype.log = function(level, txt)
{
    logger.log(level, '>> ' + txt);
};

///////////////////////////////////////////////////////////////////////////////


// export module
module.exports = Logs;
