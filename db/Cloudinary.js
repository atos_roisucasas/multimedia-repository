/**
 * http://cloudinary.com/documentation/node_integration
 */
var cloudinary = require('cloudinary');

// QUERIES
function Cloudinary(cloudname, apikey, apisecret) {
    // get connection
    cloudinary.config({
        cloud_name: cloudname,
        api_key: apikey,
        api_secret: apisecret
    });
}

// upload content
Cloudinary.prototype.createDocument = function(req, res)
{
    console.log("Uploading document to Cloudinary ... ");

    cloudinary.uploader.upload(req.body.url, function(result) {
        //console.log(result);
        res.json(result);
    }, {
        resource_type: req.body.type,
        public_id: req.body.name,
    });
};

// delete content
//     ==> http://cloudinary.com/documentation/upload_videos#api_example_10
Cloudinary.prototype.deleteDocument = function(req, res)
{
    console.log("Deleting document from Cloudinary ... " + req.params.id);
    cloudinary.uploader.destroy(req.params.id.toString(), function(result) {
            console.log(result) }, { resource_type: "video" });
};



// Get all videos
Cloudinary.prototype.getAllVideos = function(req, res)
{
    cloudinary.api.resources(function(result) {
        res.json(result);
    }, {
        resource_type: 'video'
    });
};

// Get all images
Cloudinary.prototype.getAllImages = function(req, res)
{
    cloudinary.api.resources(function(result) {

        res.json(result);
    });
};


// initialize table
Cloudinary.prototype.initialize = function() {
    /*
    cloudinary.api.resources(function(result) {
        console.log(result);
    }, {
        resource_type: 'video'
    });
    */
}



// export module
module.exports = Cloudinary;
