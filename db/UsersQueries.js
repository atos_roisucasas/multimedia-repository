var jwt = require("jsonwebtoken");
var token_var = "";

// model / schema
var Users = require('./schema/Users');

// logs
var Logs = require('./Logs');
var logs = new Logs();

// UsersQueries constructor
function UsersQueries(sc) {
    token_var = sc;
}

// public functions
// 1. USERS ///////////////////////////////////////////////////////////////////
// 1.1. get all users
UsersQueries.prototype.getAllUsers = function(req, res)
{
    Users.find({}, function(err, users) {
        if(err) {
            logs.error(err);
            res.send(err);
        }
        else {
            res.json(users);
        }
    });
};

// 1.2. get user by id
UsersQueries.prototype.getUserById = function(req, res)
{
    Users.findById(req.params.id, '-password', function(err, user) {
        if(err) {
            logs.error(err);
            res.send(err);
        }
        else {
            res.json(user);
        }
    });
};

// 1.3. create new user
UsersQueries.prototype.createUser = function(req, res)
{
    Users.create({
        name: req.body.name,
        username: req.body.username,
        password: req.body.password,
        rol: req.body.rol,
        location: req.body.location,
        done: false
    }, function(err, user){
        if(err) {
            logs.error(err);
            res.send(err);
        }
        else {
            Users.find(function(err, users) {
                if(err){
                    logs.error(err);
                    res.send(err);
                }
                else {
                    res.json(users);
                }
            });
        }
    });
};

// 1.4. update user
UsersQueries.prototype.updateUser = function(req, res)
{
    var updates = { $set:
                   {
                       name: req.body.name,
                       //username: req.body.username,
                       //user.password = user.password;
                       rol: req.body.rol,
                       location: req.body.location
                   }
                  };

    var query = {_id : {$eq: req.body._id}};

    Users.update(query, updates, function(err, user) {
        if(err) {
            logs.error(err);
            res.send(err);
        }
        else {
            Users.find(function(err, users) {
                if(err) {
                    logs.error(err);
                    res.send(err);
                }
                else {
                    res.json(users);
                }
            });
        }
    });
};

// 1.5. delete user
UsersQueries.prototype.deleteUser = function(req, res)
{
    Users.remove(
        {
            _id: req.params.id
        },
        function(err, id) {
            if(err) {
                logs.error(err);
                res.send(err);
            }
            else {
                Users.find(function(err, users) {
                    if(err) {
                        logs.error(err);
                        res.send(err);
                    }
                    else {
                        res.json(users);
                    }
                });
            }
        })
};

// 1.6. authentication
UsersQueries.prototype.doAuth = function(req, res)
{
    Users.findOne(
        {
            username: req.body.username //password: req.body.password
        },
        function(err, user) {
            if(err) {
                logs.error(err);
                res.send(err);
            }
            else if (!user) {
                res.json({ success: false, message: 'Authentication failed. User not found.' });
            }
            else {
                // test a matching password
                user.comparePassword(req.body.password, function(err, isMatch) {
                    if (err) {
                        logs.error(err);
                        res.send(err);
                    }
                    else if (isMatch) {
                        // if user is found and password is right create a token
                        var token = jwt.sign(user, token_var, {
                            expiresIn: 600 // in seconds
                        });

                        // return the information including token as JSON
                        res.json({
                            success: true,
                            message: 'Authentication succeed: Enjoy your token!',
                            token: token,
                            username: user.username,
                            rol: user.rol
                        });
                    }
                    else {
                        res.json({ success: false, message: 'Authentication failed: Invalid user / password.' });
                    }
                });
            }
        });
};


// 1.7. initialize table
UsersQueries.prototype.initialize = function(deleteAll)
{
    logs.info('Checking -Users- collection ...');

    if (deleteAll) {
        Users.remove(
            { },
            function(err, id) {
                logs.info('Deleting users ...');
                if(err) {
                    logs.error(err);
                }
            });
    }

    Users.findOne(
        {
            username: 'admin'
        },
        function(err, user) {
            if(err) {
                logs.error(err);
            }
            else if (!user) {
                logs.info('Creating admin user ...');
                Users.create({
                    name: 'administrator',
                    username: 'admin',
                    password: 'admin123',
                    rol: 'admin',
                    location: 'Germany',
                    done: false
                }, function(err, user){
                    if(err) {
                        logs.error(err);
                    }
                    else {
                        Users.find(function(err, users) {
                            if(err){
                                console.log('>> Error: ' + err);
                            }
                            logs.info('Admin user created!');
                        });
                    }
                });
            }

           logs.info('-Users- collection checked!');
        });
};



// export module
module.exports = UsersQueries;
