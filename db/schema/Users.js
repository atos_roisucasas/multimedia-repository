// USERS //////////////////////////////////////////////////////////////////////
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SALT_WORK_FACTOR = 10;
var bcrypt = require('bcrypt');   // http://stackoverflow.com/questions/14588032/mongoose-password-hashing
                                  // https://github.com/ncb000gt/node.bcrypt.js

// USERS SCHEMA
var usersSchema = new Schema({
	name: String,
	username: { type: String, required: true, unique: true },
	password: { type: String, required: true }, // select: false
	rol: String,
	location: String,
	meta: {
		age: Number,
		website: String
	},
	created_at: Date,
	updated_at: Date
});

// Custom operations...
// on every save, hash passwords and add the date
usersSchema.pre('save', function(next) {
  var user = this;

  // salt / hash / date values
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err)
      console.log('>> Error: ' + err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, function(err, hash) {
      if (err)
        console.log('>> Error: ' + err);

      // override the cleartext password with the hashed one
      user.password = hash;

      // get the current date
      var currentDate = new Date();

      // change the updated_at field to current date
      user.updated_at = currentDate;

      // if created_at doesn't exist, add to that field
      if (!this.created_at)
        user.created_at = currentDate;

      next();
    });
  });
});

// compare password with hash
usersSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

// create a model
var Users = mongoose.model('Users', usersSchema);

// make this model available
module.exports = Users;
