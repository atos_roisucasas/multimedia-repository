// DOCUMENTS //////////////////////////////////////////////////////////////////
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// DOCUMENTS SCHEMA
var documentsSchema = new Schema({
    name: { type: String, required: true, unique: true },
    desc: String,
    url: String,
    type: String,
    tags: String,
    stored: Boolean,
    created_at: Date,
    updated_at: Date
});

// Custom operations...
// on every save, add the date
documentsSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;

    next();
});

// create a model
var Documents = mongoose.model('Documents', documentsSchema);

// make this model available
module.exports = Documents;
