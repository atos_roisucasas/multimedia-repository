// model / schema
var Documents = require('./schema/Documents');

// logs
var Logs = require('./Logs');
var logs = new Logs();

// DocumentsQueries constructor
function DocumentsQueries() {}

// public functions
// 2. MULTIMEDIA //////////////////////////////////////////////////////////////
// 2.1. get all docs
DocumentsQueries.prototype.getAllDocuments = function(req, res)
{
    Documents.find({}, function(err, docs) {
        if(err) {
            logs.error(err);
            res.send(err);
        }
        else {
            res.json(docs);
        }
    });
};

// 2.2. get doc by name
DocumentsQueries.prototype.getDocumentById = function(req, res)
{
    logs.debug('2.....' + req.params.id);
    Documents.findById(req.params.id, function(err, doc) {
        if(err) {
            logs.error(err);
            res.send(err);
        }
        else {
            res.json(doc);
        }
    });
};

// 2.3. get docs
DocumentsQueries.prototype.getDocumentsByType = function(type_value, res)
{
    Documents.find({
        type: type_value
    }, function(err, docs) {
        if(err) {
            logs.error(err);
            res.send(err);
        }
        else {
            res.json(docs);
        }
    });
};

// 2.4. create new document
DocumentsQueries.prototype.createDocument = function(req, res)
{
    Documents.create({
        name: req.body.name,
        desc: req.body.desc,
        url: req.body.url,
        type: req.body.type,
        tags: req.body.tags,
        stored: req.body.stored,
        done: false
    }, function(err, doc){
        if(err) {
            logs.error(err);
            res.send(err);
        }
        else {
            Documents.find({}, function(err, docs) {
                if(err) {
                    logs.error(err);
                    res.send(err);
                }
                else {
                    res.json(docs);
                }
            });
        }
    });

};


// 2.5. delete document
DocumentsQueries.prototype.deleteDocument = function(req, res)
{
    Documents.remove(
        {
            _id: req.params.id
        },
        function(err, id) {
            if(err) {
                logs.error(err);
                res.send(err);
            }
            else {
                Documents.find({}, function(err, docs) {
                    if(err) {
                        logs.error(err);
                        res.send(err);
                    }
                    else {
                        res.json(docs);
                    }
                });
            }
        })
};


// initialize table
DocumentsQueries.prototype.initialize = function(deleteAll)
{
    logs.info('Checking -Documents- collection ...');
    if (deleteAll) {
        Documents.remove(
            { },
            function(err, id) {
                logs.info('Deleting documents ...');
                if(err) {
                    logs.error(err);
                }
            });
    }

    Documents.find(
        {},
        function(err, docs) {
            if(err) {
                logs.error(err);
            }
            else if (!docs || docs.length == 0) {
                logs.info('Creating test document ...');
                Documents.create({
                    name: 'Alzheimers001',
                    desc: 'Experience 12 Minutes In Alzheimers Dementia',
                    url: 'https://youtu.be/LL_Gq7Shc-Y',
                    type: 'video',
                    tags: 'dementia',
                    stored: false,
                    done: false
                }, function(err, docs){
                    if(err) {
                        logs.error(err);
                    }
                    else {
                        Documents.findOne({
                            name: 'Alzheimers001'
                        }, function(err, doc) {
                            if(err){
                                logs.error(err);
                            }
                            else {
                                logs.info('Test document created!');
                            }
                        });
                    }
                });
            }

            logs.info('-Documents- collection checked!');
        });
};


// export module
module.exports = DocumentsQueries;
