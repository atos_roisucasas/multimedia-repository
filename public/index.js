angular.module('angular-app', ['ngStorage', 'chart.js'])


.config(function($provide, $httpProvider) {
    // Intercept http calls
    $provide.factory('httpRequestInterceptor', ['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
        return {
            'request': function (config)
            {
                config.headers = config.headers || {};

                if ($localStorage.token) {
                    config.headers.Authorization = $localStorage.token;
                }

                return config;
            },

            'responseError': function (response)
            {
                if (response.status === 401 || response.status === 403) {
                    $location.path('/signin');
                }

                return $q.reject(response);
            }
        };
    }]);

    // Add the interceptor to the $httpProvider.
    $httpProvider.interceptors.push('httpRequestInterceptor');
})


.run(function() {

})


.controller('mainController', function($scope, $log, $http, $localStorage) {
    // scope
    $scope.showModal = false;
    $scope.user = {
        username: null,
        logged: false,
        password: null,
        rol: null
    };
    $scope.content= {
        active: false,
        url: 'main.html',
        current: 'Home'
    };

    // fill scope content if user already logged
    if ($localStorage.token != null) {
        $log.debug('>> User already logged ...');
        try {
            $scope.user.password = null;
            $scope.user.username = $localStorage.user.username;
            $scope.user.rol = $localStorage.user.rol;
            $scope.user.logged = true;

            $scope.showModal = $localStorage.showModal;

            $scope.content.url = $localStorage.content.url;
            $scope.content.current = $localStorage.content.current;
        }
        catch(err) {
            $log.error(err)
        }
    }

    ///// $localStorage //////
    $log.debug('>> Initialize $localStorage to store $scope content ... ');
    // token
    // showModal
    $localStorage.showModal = $scope.showModal;
    // user
    $localStorage.user = $scope.user;
    // content
    $localStorage.content = $scope.content;



    /* functions ***********************/
    // FUNCTION:
    $scope.toggleModal = function() {
        $scope.showModal = !$scope.showModal;
    };

    // FUNCTION: login
    $scope.login = function () {
        var dataObj = {
            username : $scope.user.username,
            password : $scope.user.password
        };

        $http.post('/api/auth', dataObj).success(function(data) {
            if ((data != null) && (data.success))
            {
                $localStorage.token = data.token;

                $scope.user.password = null;
                $scope.user.username = data.username;
                $scope.user.rol = data.rol;
                $scope.user.logged = true;

                $scope.showModal = false;

                if ($scope.user.rol == 'admin') {
                    $scope.content.url = 'users.html';
                    $scope.content.current = 'Users';
                }
                else {
                    $scope.content.url = 'multimedia.html';
                    $scope.content.current = 'Multimedia Content';
                }
            }
            else
            {
                $log.debug("User not authenticated");
            }
        })
        .error(function() {
            $log.error('Error [/auth/login] !!!!');
        });
    };

    // MENU:
    // FUNCTION: go to users
    $scope.goUsers = function() {
        $scope.content.url = 'users.html';
        $scope.content.current = 'Users';
    };

    // FUNCTION: go to multimedia content
    $scope.goMContent = function() {
        $scope.content.url = 'multimedia.html';
        $scope.content.current = 'Multimedia Content';
    };

    // FUNCTION: go to monitoring
    $scope.goMonitoring = function() {
        $scope.content.url = 'monitoring.html';
        $scope.content.current = 'Monitoring';
    };

    // FUNCTION: logout
    $scope.logout = function() {
        $log.debug('>> Log out ... ');
        $localStorage.token = null;

        $scope.user.username = null;
        $scope.user.logged = false;
        $scope.user.rol = null;
        $scope.user.password = null;

        $scope.content.url = 'main.html';
        $scope.content.current = 'Home';
    };
})


.directive('ngConfirmBoxClick', [
    function () {
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmBoxClick || "Are you sure want to delete?";
                var clickAction = attr.confirmedClick;
                element.bind('click', function (event) {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    }
])


.directive('modal', function () {
    return {
        template:
        '<div class="modal fade">' +
        '  <div class="modal-dialog modal-sm">' +
        '    <div class="modal-content">' +
        '      <div class="modal-header">' +
        '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
        '        <h4 class="modal-title">{{ title }}</h4>' +
        '      </div>' +
        '      <div class="modal-body" ng-transclude></div>' +
        '    </div>' +
        '  </div>' +
        '</div>',
        restrict: 'E',
        transclude: true,
        replace:true,
        scope:true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;

            scope.$watch(attrs.visible, function(value){
                if(value == true)
                    $(element).modal('show');
                else
                    $(element).modal('hide');
            });

            $(element).on('shown.bs.modal', function(){
                scope.$apply(function(){
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function(){
                scope.$apply(function(){
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});
