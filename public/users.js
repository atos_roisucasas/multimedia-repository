angular.module('angular-app')

.controller('usersController', function($scope, $log, $http) {
    // FORM
    $scope.formData = {
        name: "",
        username: "",
        password: "",
        rol: "",
        location: ""
    };
    var originalForm = angular.copy($scope.formData2);

    $scope.selectedID = null;
    $scope.new_user = false;

    // 1st page loading... Load all users
    $scope.loading = true;

    $http.get('/api/users').success(function(data) {
        $scope.users = data;
    })
    .error(function(data) {
        console.log(data);
        console.log('Error: ' + data);
    })
    .finally(function() {
        $scope.loading = false;
    });

    // form disabled
    $scope.formEnabled = false;

    ///////////////////////////////////////////////////////////////////////////
    // FUNCTION:
    $scope.enableForm = function() {
        $scope.formData = angular.copy(originalForm);
        $scope.selectedID = null;

        $scope.formEnabled = true;
        $scope.new_user = true;
    };

    // FUNCTION: cancel and close form
    $scope.cancel = function() {
        $scope.clearForm();
        $scope.formEnabled = false;
        $scope.new_user = false;
    };

    // FUNCTION: clear form
    $scope.clearForm = function() {
        $scope.formData = angular.copy(originalForm);
        $scope.selectedID = null;

        $scope.usersForm.$setPristine();
        $scope.usersForm.$setValidity();
    };

    // FUNCTION: Show data from user
    $scope.showUserData = function(id) {
        $scope.loading = true;

        $http.get('/api/users/' + id).success(function(data) {
            $scope.formData = data;
            $scope.selectedID = id;
        })
        .error(function(data) {
            console.log('Error: ' + data);
        })
        .finally(function() {
            $scope.loading = false;
        });

        $scope.formEnabled = true;
    };

    // FUNCTION: Creates new user
    $scope.createUser = function() {
        $scope.loading = true;

        $http.post('/api/users', $scope.formData).success(function(data) {
            $scope.formData = {};
            $scope.users = data;

            $scope.cancel();
        })
        .error(function(data) {
            console.log('Error:' + data);
        })
        .finally(function() {
            $scope.loading = false;
        });
    };

    // FUNCTION: Updates user
    $scope.updateUser = function() {
        if ($scope.selectedID == null) {
            $log.error('>> Error: $scope.selectedID == null');
        }
        else {
            $scope.loading = true;

            $http.put('/api/users/' + $scope.selectedID, $scope.formData).success(function(data) {
                $scope.formData = {};
                $scope.users = data;
            })
            .error(function(data) {
                console.log('Error:' + data);
            })
            .finally(function() {
                $scope.loading = false;
            });
        }
    };

    // FUNCTION: Deletes user by id
    $scope.deleteUser = function(id) {
        $scope.loading = true;

        $http.delete('/api/users/' + id).success(function(data) {
            $scope.users = data;
        })
        .error(function(data) {
            console.log('Error:' + data);
        })
        .finally(function() {
            $scope.loading = false;
        });

        $scope.formData = {};
        $scope.selectedID = null;
    };

});
