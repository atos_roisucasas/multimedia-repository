angular.module('angular-app')

.controller('multimediaController', function($scope, $log, $http) {
    // FORM
    $scope.formDataMedia = {
        name: "",
        desc: "",
        url: "",
        type: "",
        tags: "",
        stored: ""
    };
    var originalForm = angular.copy($scope.formDataMedia);

    $scope.selectedID = null;
    $scope.new_doc = false;

    // scope vars:
    $scope.mcontent= {
        type: 'ALL'
    };
    $scope.docs = {};

    // 1st page loading... Load all documents
    $scope.docs = {};
    $scope.loading = true;
    $http.get('/api/videos').success(function(data) {
        $scope.docs = data;
        $scope.mcontent.type = "VIDEOS";
    })
    .error(function(data) {
        $log.error('Error: ' + data);
    })
    .finally(function() {
        $scope.loading = false;
    });

    // form disabled
    $scope.formEnabled = false;

    ///////////////////////////////////////////////////////////////////////////
    // FUNCTION:
    $scope.enableForm = function() {
        $scope.formDataMedia = angular.copy(originalForm);
        $scope.selectedID = null;

        $scope.formEnabled = true;
        $scope.new_doc = true;

        $scope.formDataMedia.type = "video";
    };


    // FUNCTION: cancel and close form
    $scope.cancel = function() {
        $scope.clearForm();
        $scope.formEnabled = false;
        $scope.new_doc = false;
    };


    // FUNCTION: clear form
    $scope.clearForm = function() {
        $scope.formDataMedia = angular.copy(originalForm);
        $scope.selectedID = null;

        //$scope.usersForm.$setPristine();
        //$scope.usersForm.$setValidity();
    };


    // FUNCTION: Creates new document
    $scope.createDocument = function() {
        $scope.loading = true;

        var valStored = ($scope.formDataMedia.stored === "true");
        $scope.formDataMedia.name = $scope.formDataMedia.name.replace(/\s/g, '_');
        if (valStored) {
            // store in Cloudinary
            $log.debug('>> 1. Storing in Cloudinary ... ');

            $http.post('/api/cloud/documents', $scope.formDataMedia).success(function(data) {
                $log.debug(data);

                // store in db with the url of document in cloudinary
                $log.debug('>> 2. Storing document in DB ... ');

                $scope.formDataMedia.url = data.url;

                $http.post('/api/documents', $scope.formDataMedia).success(function(data) {
                    $scope.docs = data;
                    $log.debug(data);
                })
                .error(function(data) {
                    $log.error('>> Error:' + data);
                });
            })
            .error(function(data) {
                $log.error('>> Error:' + data);
            })
            .finally(function() {
                $scope.formDataMedia = {};
                $scope.loading = false;
            });
        }
        else {
            // store in db with the given url
            $log.debug('>> 1. Storing document in DB ... ');

            $http.post('/api/documents', $scope.formDataMedia).success(function(data) {
                $scope.formDataMedia = {};
                $scope.docs = data;
                $log.debug(data);
            })
            .error(function(data) {
                $log.error('>> Error:' + data);
            })
            .finally(function() {
                $scope.loading = false;
            });
        }
    };

    // FUNCTION:
    $scope.showDocumentData = function(id) {
        $scope.loading = true;

        $http.get('/api/documents/' + id).success(function(data) {
            $scope.selectedID = id;
            $scope.formDataMedia = data;
            if (data.stored) {
                $scope.formDataMedia.stored = "true";
            }
            else {
                $scope.formDataMedia.stored = "false";
            }

            $scope.formEnabled = true;
        })
        .error(function(data) {
            $log.error('Error: ' + data);
        })
        .finally(function() {
            $scope.loading = false;
        });
    }

    // FUNCTION:
    $scope.deleteDocument = function(id) {
        $scope.loading = true;

        $http.get('/api/documents/' + id).success(function(data) {
            if (data.stored) {
                alert(data.name);
                // delete from cloudinary
                $http.delete('/api/cloud/document/' + data.name).success(function(data) {
                    console.log('Info: document deleted from Cloudinary');
                })
                .error(function(data) {
                    console.log('Error:' + data);
                });
            }
        })
        .error(function(data) {
            $log.error('Error: ' + data);
        });

        // delete from database
        $http.delete('/api/documents/' + id).success(function(data) {
            $scope.docs = data;
        })
        .error(function(data) {
            console.log('Error:' + data);
        })
        .finally(function() {
            $scope.loading = false;
        });

        $scope.formDataMedia = {};
        $scope.selectedID = null;
    }

    // FUNCTION: get all documents
    $scope.getAll = function(id) {
        $scope.docs = {};
        $scope.loading = true;

        $http.get('/api/documents').success(function(data) {
            $scope.docs = data;
        })
        .error(function(data) {
            $log.error('Error: ' + data);
        })
        .finally(function() {
            $scope.loading = false;
        });
    };

    // FUNCTION:
    $scope.getVideos = function(id) {
        $scope.docs = {};
        $scope.loading = true;
        $scope.mcontent.type = "VIDEOS";

        $http.get('/api/videos').success(function(data) {
            $scope.docs = data;
        })
        .error(function(data) {
            $log.error('Error: ' + data);
        })
        .finally(function() {
            $scope.loading = false;
        });
    }

    // FUNCTION:
    $scope.getImages = function(id) {
        $scope.docs = {};
        $scope.loading = true;
        $scope.mcontent.type = "IMAGES";

        $http.get('/api/images').success(function(data) {
            $scope.docs = data;
        })
        .error(function(data) {
            $log.error('Error: ' + data);
        })
        .finally(function() {
            $scope.loading = false;
        });
    }

    // FUNCTION:
    $scope.getBooks = function(id) {
        $scope.docs = {};
        $scope.loading = true;
        $scope.mcontent.type = "BOOKS";

        $http.get('/api/books').success(function(data) {
            $scope.docs = data;
        })
        .error(function(data) {
            $log.error('Error: ' + data);
        })
        .finally(function() {
            $scope.loading = false;
        });
    }

    // FUNCTION:
    $scope.getOthers = function(id) {
        $scope.docs = {};
        $scope.loading = true;
        $scope.mcontent.type = "OTHER";

        $http.get('/api/others').success(function(data) {
            $scope.docs = data;
        })
        .error(function(data) {
            $log.error('Error: ' + data);
        })
        .finally(function() {
            $scope.loading = false;
        });
    }


     // FUNCTION:
    $scope.getMusic = function(id) {
        $scope.docs = {};
        $scope.loading = true;
        $scope.mcontent.type = "MUSIC";

        $http.get('/api/music').success(function(data) {
            $scope.docs = data;
        })
        .error(function(data) {
            $log.error('Error: ' + data);
        })
        .finally(function() {
            $scope.loading = false;
        });
    }

    // FUNCTION:
    $scope.updateDocument = function() {

    }

});
