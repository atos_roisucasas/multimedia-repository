angular.module('angular-app')

.controller('monitoringController', function($scope, $http, $interval) {
    $scope.containerObject = {
        resp_txt: '',
        resp_txt2: '',
        freeMem: 0,
        totalMem: 0,
        appMem: 0,
        appProcessMem: 0,
        loadAvg: 0
    };

    // GET DATA ///////////////////////////////////////////////////////
    $scope.doCallToREST = function() {
        $scope.containerObject.resp_txt = '.....';

        $http.get('/monitor/data').success(function(data) {
            $scope.todos = data;
            $scope.containerObject.resp_txt = 'value: ' + data.value;

            var strData = '';
            var totalData = data.result.length;

            if (totalData > 0)
            {
                console.log('total: ' + totalData);

                for (i=0; i<totalData; i++)
                {
                    strData += ' ' + data.result[i].value;
                }
            }
            else
            {
                console.log('no results!!!!');
            }

            $scope.containerObject.resp_txt = '.....' + totalData + '\n.....' + strData;
        })
        .error(function(data) {
            $scope.containerObject.resp_txt = 'Error: ' + data;
            console.log('Error: ' + data);
        });
    };

    // STOP MONITOR ///////////////////////////////////////////////////
    $scope.stopMonitor = function() {
        $http.get('/monitor/stop').success(function() {
            if (angular.isDefined(stopChart)) {
                $interval.cancel(stopChart);
                stopChart = undefined;
                console.log('stopMonitor: Monitor stopped.');
            }
        })
        .error(function() {
            console.log('stopMonitor: Error !!!!');
        });
    };

    // CHARTS /////////////////////////////////////////////////////////
    // 1 - memory usage
    $scope.labels = ['10', '9', '8', '7', '6', '5', '4', '3', '2', '1'];
    $scope.series = ['Free Memory', 'Node App', 'Java App'];
    $scope.data = [ [], [], []];

    // 2 - cpu
    $scope.labels2 = ['average: last 1 min.', 'idle'];
    $scope.data2 =  [ 0, 0 ];

    for (var j=0; j<10; j++)
    {
        $scope.data[0][j] = 0;
        $scope.data[1][j] = 0;
        $scope.data[2][j] = 0;
    }

    var stopChart;
    $scope.startMonitor = function()
    {
        // don't start a new task if we have one task running
        if ( angular.isDefined(stopChart) )
            return;

        stopChart = $interval(function()
                              {
            $http.get('/monitor/data').success(function(data) {
                for (var i=0; i<data.result.length; i++) {
                    for (var name in data.result[i]) {
                        if ("freeMemArr" == name)
                        {
                            $scope.data[0] = data.result[i][name];
                            $scope.containerObject.freeMem = $scope.data[0][9];
                        }
                        else if  ("totalMemArr" == name)
                        {
                            $scope.containerObject.totalMem = data.result[i][name][9];
                        }
                        else if  ("appMemArr" == name)
                        {
                            $scope.data[2] = data.result[i][name];
                            $scope.containerObject.appMem = $scope.data[2][9];
                        }
                        else if  ("appProcessMemArr" == name)
                        {
                            //console.log('/!!!!' + data.result[i][name][9]);
                            //$scope.containerObject.appProcessMem = data.result[i][name][9];

                            $scope.data[3] = data.result[i][name];
                            $scope.containerObject.appProcessMem = $scope.data[3][9];
                        }
                        else if  ("loadavg" == name)
                        {
                            $scope.data2[0] = data.result[i][name][9][0] * 100;
                            $scope.data2[0] = Number(Math.round($scope.data2[0]+'e'+2)+'e-'+2);

                            $scope.data2[1] = 100 - $scope.data2[0];
                            $scope.containerObject.loadAvg = $scope.data2[0];
                        }

                        $scope.containerObject.resp_txt = $scope.containerObject.appMem + "\n" + $scope.containerObject.appProcessMem;
                    }
                }
            })
            .error(function() {
                console.log('/data-monitor ERROR !!!!');
            });
        }, 2500);
    };


    // start tasks
    //$scope.startMonitor();
})

// EXAMPLE CONTROLLER
.controller('ExampleController', function($scope, $interval) {
    $scope.format = 'dd/MM/yyyy h:mm:ss a';
})

// DIRECTIVE
// Register the 'myCurrentTime' directive factory method.
// We inject $interval and dateFilter service since the factory method is DI.
.directive('myCurrentTime', ['$interval', 'dateFilter', function($interval, dateFilter) {
    // return the directive link function. (compile function not needed)
    return function(scope, element, attrs) {
        var format,  // date format
            stopTime; // so that we can cancel the time updates

        // used to update the UI
        function updateTime() {
            element.text(dateFilter(new Date(), format));
        }

        // watch the expression, and update the UI on change.
        scope.$watch(attrs.myCurrentTime, function(value) {
            format = value;
            updateTime();
        });

        stopTime = $interval(updateTime, 1000);

        // listen on DOM destroy (removal) event, and cancel the next UI update
        // to prevent updating time after the DOM element was removed.
        element.on('$destroy', function() {
            $interval.cancel(stopTime);
        });
    }
}]);
